/*
 * Copyright 2015-2019 Autoware Foundation. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <waypoint_planner/velocity_set/velocity_set_info.h>

VelocitySetInfo::VelocitySetInfo()
  : private_nh_("~"), nh(""), tf2_listener_(tf2_buffer_)
{

  wpidx_detectionResultByOtherNodes_ = -1;
  set_pose_ = false;

  double vel_change_limit_kph = 9.972;
  private_nh_.param<double>("remove_points_upto", remove_points_upto_, 2.3);
  private_nh_.param<double>("stop_distance_obstacle", stop_distance_obstacle_, 10.0);
  private_nh_.param<double>("stop_distance_stopline", stop_distance_stopline_, 5.0);
  private_nh_.param<double>("detection_range", stop_range_, 1.3);
  private_nh_.param<int>("points_threshold", points_threshold_, 10);
  private_nh_.param<double>("detection_height_top", detection_height_top_, 0.2);
  private_nh_.param<double>("detection_height_bottom", detection_height_bottom_, -1.7);
  private_nh_.param<double>("deceleration_obstacle", deceleration_obstacle_, 0.8);
  private_nh_.param<double>("deceleration_stopline", deceleration_stopline_, 0.6);
  private_nh_.param<double>("velocity_change_limit", vel_change_limit_kph, 9.972);
  private_nh_.param<double>("deceleration_range", deceleration_range_, 0);
  private_nh_.param<double>("temporal_waypoints_size", temporal_waypoints_size_, 100.0);

  velocity_change_limit_ = vel_change_limit_kph / 3.6;  // kph -> mps

  health_checker_ptr_ = std::make_shared<autoware_health_checker::HealthChecker>(nh,private_nh_);
  health_checker_ptr_->ENABLE();
  
}

void VelocitySetInfo::clearPoints()
{
  points_.clear();
}

void VelocitySetInfo::configCallback(const autoware_config_msgs::ConfigVelocitySetConstPtr &config)
{
  stop_distance_obstacle_ = config->stop_distance_obstacle;
  stop_distance_stopline_ = config->stop_distance_stopline;
  stop_range_ = config->detection_range;
  points_threshold_ = config->threshold_points;
  detection_height_top_ = config->detection_height_top;
  detection_height_bottom_ = config->detection_height_bottom;
  deceleration_obstacle_ = config->deceleration_obstacle;
  deceleration_stopline_ = config->deceleration_stopline;
  velocity_change_limit_ = config->velocity_change_limit / 3.6; // kmph -> mps
  deceleration_range_ = config->deceleration_range;
  temporal_waypoints_size_ = config->temporal_waypoints_size;
}

bool VelocitySetInfo::TransformPointCloud(const std::string& in_target_frame,
                                          const sensor_msgs::PointCloud2::ConstPtr& in_cloud_ptr,
                                          const sensor_msgs::PointCloud2::Ptr& out_cloud_ptr)
{
  if (in_target_frame == in_cloud_ptr->header.frame_id)
  {
    *out_cloud_ptr = *in_cloud_ptr;
    return true;
  }


  geometry_msgs::TransformStamped transform_stamped;
  try
  {
    transform_stamped = tf2_buffer_.lookupTransform(in_target_frame, in_cloud_ptr->header.frame_id,
                                                   in_cloud_ptr->header.stamp, ros::Duration(1.0));
  }
  catch (tf2::TransformException& ex)
  {
    ROS_WARN("%s", ex.what());
    return false;
  }
  // tf2::doTransform(*in_cloud_ptr, *out_cloud_ptr, transform_stamped);
  Eigen::Matrix4f mat = tf2::transformToEigen(transform_stamped.transform).matrix().cast<float>();
  pcl_ros::transformPointCloud(mat, *in_cloud_ptr, *out_cloud_ptr);
  out_cloud_ptr->header.frame_id = in_target_frame;
  return true;
}

/*

void VelocitySetInfo::pointsCallback(const sensor_msgs::PointCloud2ConstPtr &msg)
{
  health_checker_ptr_->CHECK_RATE("topic_rate_points_no_ground_slow", 8, 5, 1, "topic points_no_ground subscribe rate slow.");
  points_.clear();
    
    
  std::string base_frame_ = "base_link";    
    
  sensor_msgs::PointCloud2::Ptr trans_sensor_cloud(new sensor_msgs::PointCloud2);
  const bool succeeded = VelocitySetInfo::TransformPointCloud(base_frame_, msg, trans_sensor_cloud);
  if (!succeeded)
  {
    ROS_ERROR_STREAM_THROTTLE(10, "Failed transform from " << base_frame_ << " to " << msg->header.frame_id);
    return;
  }  
  
  
  pcl::PointCloud<pcl::PointXYZ> sub_points;
  pcl::fromROSMsg(*msg, sub_points);  
  
  for (const auto &v : sub_points)
  {
    if (v.x == 0 && v.y == 0)
      continue;

    if (v.z > detection_height_top_ || v.z < detection_height_bottom_)
      continue;

    // ignore points nearby the vehicle
    if (v.x * v.x + v.y * v.y < remove_points_upto_ * remove_points_upto_)
      continue;

    points_.push_back(v);
  }

}

*/


void VelocitySetInfo::pointsCallback(const sensor_msgs::PointCloud2ConstPtr &in_sensor_cloud)
{
  health_checker_ptr_->CHECK_RATE("topic_rate_points_no_ground_slow", 8, 5, 1, "topic points_no_ground subscribe rate slow.");

  points_.clear();


  std::string base_frame_ = "base_link";

  sensor_msgs::PointCloud2::Ptr trans_sensor_cloud(new sensor_msgs::PointCloud2);
  const bool succeeded = VelocitySetInfo::TransformPointCloud(base_frame_, in_sensor_cloud, trans_sensor_cloud);
  if (!succeeded)
  {
    ROS_ERROR_STREAM_THROTTLE(10, "Failed transform from " << base_frame_ << " to "
                                                           << in_sensor_cloud->header.frame_id);
    return;
  }
  
  pcl::PointCloud<pcl::PointXYZ>::Ptr sub_points(new pcl::PointCloud<pcl::PointXYZ>);  
  pcl::fromROSMsg(*trans_sensor_cloud, *sub_points);
  
  pcl::ExtractIndices<pcl::PointXYZ> extractor;
  extractor.setInputCloud(sub_points);
  pcl::PointIndices indices;

#pragma omp for
  for (size_t i = 0; i < sub_points->points.size(); i++)
  {
    if (sub_points->points[i].z >= detection_height_bottom_ && sub_points->points[i].z <= detection_height_top_ && (sub_points->points[i].x * sub_points->points[i].x + sub_points->points[i].y * sub_points->points[i].y) >= remove_points_upto_ * remove_points_upto_)
    {
      indices.indices.push_back(i);
    }
  }
  extractor.setIndices(boost::make_shared<pcl::PointIndices>(indices));
  extractor.setNegative(false);  // true removes the indices, false leaves only the indices
  extractor.filter(points_);
}


void VelocitySetInfo::detectionCallback(const std_msgs::Int32 &msg)
{
    wpidx_detectionResultByOtherNodes_ = msg.data;
}

void VelocitySetInfo::controlPoseCallback(const geometry_msgs::PoseStampedConstPtr &msg)
{
  control_pose_ = *msg;
  health_checker_ptr_->NODE_ACTIVATE();
  health_checker_ptr_->CHECK_RATE("topic_rate_control_pose_slow", 8, 5, 1, "topic localizer_pose subscribe rate slow.");

  if (!set_pose_)
    set_pose_ = true;
}

void VelocitySetInfo::localizerPoseCallback(const geometry_msgs::PoseStampedConstPtr &msg)
{

  localizer_pose_ = *msg;
}
